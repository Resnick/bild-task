function complete = writing_result(testOptions,numRepetition,results,subjectname)

format long
beep off

time = fix(clock);
today = date;
hour = num2str(time(4));
minute = num2str(time(5));
angle = round(testOptions{1}*180/pi());
cueType = testOptions{2};
noiseType = testOptions{3};

numRev = length(results(1).total);
trialMean = nan(numRepetition,1);
trialStd = nan(numRepetition,1);
totalRev = nan(numRepetition,numRev);
go = nan(numRepetition,76);
count= nan(numRepetition,76);

dataDir = ['Data',filesep,subjectname,filesep,today,filesep];
if angle == 0
    cueType = 'None';
end
testName = sprintf('%s_%s_Angle_%+03.0f',noiseType,cueType,angle);
for repNum = 1:numRepetition
    trialMean(repNum) = results(repNum).mean;
    trialStd(repNum) = results(repNum).std;
    totalRev(repNum,:) = results(repNum).total; %for plotting
    go(repNum,:) = results(repNum).go;
    count(repNum,:) = results(repNum).count;
    %****plotting the percent correct figure******%
    for m = 1:76
        if go(m) == 0
            go(m) = Inf;
        end
    end
%     %****plotting the percent correct figure******%
%     index = find(go(repNum,:) ~= Inf);
%     percent_correct = (count(repNum,:)./go(repNum,:))*100;
%     x_initial = index(1);
%     x_last = index(length(index));
%     xinitial = 2*x_initial - 102;
%     xlast = 2*x_last - 102;
%     x = xinitial:2:xlast;
%     y = percent_correct(index);
%     figure, plot(x,y, '-rs','LineWidth',2,'MarkerEdgeColor','y', 'MarkerFaceColor','b', 'MarkerSize',15)
%     grid on
%     xlabel('SNR (dB)');
%     ylabel('Percent correct %');
%     title('Score versus each SNR condition');
%     fullname = [dataDir,today,'_', hour, ';', minute, '_', subjectname, '_', testName, '_score_versus_each_SNR_condition', '.fig'];
%     pause(0.2);
%     print(gcf, '-dbmp', fullname);
%     
%     fullname = [dataDir,today,'_', hour, ';', minute, '_', subjectname, '_', testName, '_score_versus_each_SNR_condition', '.jpeg'];
%     pause(0.2);
%     print(gcf, '-djpeg', fullname);
end

txtfullname = [dataDir,subjectname, '_','BMLD_TEST_with_', testName,'.txt'];
fid = fopen(txtfullname,'w');
fprintf(fid,'BMLD Test %s \r\n',testName);
fprintf(fid,'\r\n');
fprintf(fid,'*Subject name: %s \r\n', subjectname);
fprintf(fid,'*Date: %s, Time: %s:%s \r\n', today, hour, minute);
fprintf(fid,'*Cue Type: %s \r\n', cueType);
fprintf(fid,'*Noise Type: %s \r\n', noiseType);
fprintf(fid,'*Angle: %s \r\n', angle);
fprintf(fid,'\r\n');
fprintf(fid,'--------------------\r\n');
fprintf(fid,'RUN    MEAN    STD\r\n');
fprintf(fid,'--------------------\r\n');

for repNum = 1:numRepetition
    fprintf('%s Mean SRT Run %d: %.2f\n',testName,repNum,trialMean(repNum));
    fprintf('%s Standard Deviation SRT Run %d: %.2f\n',testName,repNum,trialStd(repNum));
    
%     figure, plot(totalRev(repNum,:), '--rs','LineWidth',2,'MarkerEdgeColor','k', 'MarkerFaceColor','g', 'MarkerSize',10)
%     xlabel('Reverse number');
%     ylabel('dB SPL');
%     title(sprintf('%s Run %d',testName,repNum));
%     
%     fullname = [dataDir,today,'_', hour, ';', minute, '_', subjectname, testName,'_','Run',num2str(repNum), '.bmp'];
%     pause(0.2);
%     print(gcf, '-dbmp', fullname);
    
    fprintf(fid,'%d     %.2f   %.2f\r\n',repNum,trialMean(repNum),trialStd(repNum));
    fprintf(fid,'--------------------\r\n');
    
end
if numRepetition > 1
    meanOverall = mean(trialMean);
    stdTestTest = std(trialMean);
    fprintf('%s Overall Mean SRT: %.2f\n',testName,meanOverall);
    fprintf('%s Overall STD SRT: %.2f\n',testName,stdTestTest);
    fprintf(fid,'Total  %.2f   %.2f\r\n',meanOverall,stdTestTest);
end
fclose(fid);
complete = 1;
end
