function bild(varargin)
% bild M-file for bild.fig
%      bild, by itself, creates a new bild or raises the existing
%      singleton*.
%
%      H = bild returns the handle to a new bild or the handle to
%      the existing singleton*.
%
%      bild('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in bild.M with the given input arguments.
%
%      bild('Property','Value',...) creates a new bild or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bild_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bild_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright 2002-2003 The MathWorks, Inc.

% Edit the above text to modify the response to help bild

% Last Modified by GUIDE bild.5 19-Nov-2004 17:34:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @bild_OpeningFcn, ...
    'gui_OutputFcn',  @bild_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before bild is made visible.
function bild_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bild (see VARARGIN)

% Choose default command line output for bild
[thisPath,~,~] = fileparts(mfilename('fullpath'));
cd(thisPath); addpath(genpath('.'));
handles.output = hObject;
beep off
set(hObject,'Color','k')

% Update handles structure
handles.subjectName = ''; %default

handles.noiseTypes = struct();
handles.cueTypes = struct();
handles.azimuths = [];

handles.initSnr = 10;
handles.numRepetition = 1;
handles.speechLevel = 50;

handles.ConfigFile = [];

guidata(hObject, handles);
uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function bild_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in StartButton.
function StartButton_Callback(hObject, eventdata, handles)
% hObject    handle to StartButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty(handles.azimuths) || isempty(fieldnames(handles.noiseTypes))...
        || isempty(fieldnames(handles.cueTypes)) || strcmp(handles.subjectName,'')
    warning('No complete tasks selected');
    uiwait(handles.figure1)
else
    uiresume(handles.figure1)
    delete(handles.figure1);
    play_Selected(handles.subjectName, handles.initSnr,...
        handles.numRepetition, handles.speechLevel,...
        handles.noiseTypes, handles.azimuths, handles.cueTypes);
end


% --- Executes during object creation, after setting all properties.
function NameBox_CreateFcn(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to NameBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

function NameBox_Callback(hObject, eventdata, handles)  %#ok<*DEFNU>
% hObject    handle to NameBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NameBox as text
%        str2double(get(hObject,'String')) returns contents of NameBox as a double
handles.subjectName = get(hObject,'String');
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function InitialSNRpopUp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InitialSNRpopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% --- Executes on selection change in InitialSNRpopUp.
function InitialSNRpopUp_Callback(hObject, eventdata, handles)
% hObject    handle to InitialSNRpopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns InitialSNRpopUp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from InitialSNRpopUp
val = get(hObject,'Value');
str = get(hObject, 'String');
switch str{val}
    case '+ 0'
        handles.initSnr = 0;
    case '+10'
        handles.initSnr = 10;
    case '+20'
        handles.initSnr = 20;
end
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function NumRepsPopUp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NumRepsPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end

% --- Executes on selection change in NumRepsPopUp.
function NumRepsPopUp_Callback(hObject, eventdata, handles)
% hObject    handle to NumRepsPopUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns NumRepsPopUp contents as cell array
%        contents{get(hObject,'Value')} returns selected item from NumRepsPopUp
val = get(hObject,'Value');
str = get(hObject, 'String');
switch str{val}
    case '1'
        handles.numRepetition = 1;
    case '2'
        handles.numRepetition = 2;
    case '3'
        handles.numRepetition = 3;
    case '4'
        handles.numRepetition = 4;
end
guidata(hObject,handles)

% --- Executes on button press in CalibrateButton.
function CalibrateButton_Callback(hObject, eventdata, handles)
% hObject    handle to CalibrateButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% [s1,fs] = audioread('concatenate_spondee.wav');

[s1,fs] = audioread('calib_tone.wav');
ampConstant = 8.037; % Determined empirically using SLM. JR 4/9/2018
speechSPLconstant = 10^(handles.speechLevel/20)*20E-6/(ampConstant*std(s1));
sound(s1*speechSPLconstant,fs)

% --- Executes on button press in SaveButton.
function saveError = SaveButton_Callback(hObject, eventdata, handles)
% hObject    handle to SaveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ConfigDir = ['.' filesep 'ConfigFiles' filesep];
try
    [FileName,PathName] = uiputfile('*.fig','Save Configuration File As:',ConfigDir);
    handles.ConfigFile = [PathName FileName];
    savefig(handles.figure1,handles.ConfigFile)
catch
    saveError = 'No file chosen.' %#ok<NOPRT>
end

% --- Executes on button press in LoadButton.
function loadError = LoadButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ConfigDir = ['.' filesep 'ConfigFiles' filesep];
try
    [FileName,PathName] = uigetfile('*.fig','Load Configuration File',ConfigDir);
catch
    loadError = 'No file selected' %#ok<NOPRT>
end
if FileName
    close(handles.figure1);
    openfig([PathName FileName]);
end

% --- Executes during object creation, after setting all properties.
function SpeechLevelPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SpeechLevelPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
% --- Executes on selection change in SpeechLevelPopup.

function SpeechLevelPopup_Callback(hObject, eventdata, handles)
% hObject    handle to SpeechLevelPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(hObject,'Value');
str = get(hObject, 'String');
switch str{val}
    case '50 dBA'
        handles.speechLevel = 50;
    case '55 dBA'
        handles.speechLevel = 55;
    case '60 dBA'
        handles.speechLevel = 60;
    case '65 dBA'
        handles.speechLevel = 65;
end
guidata(hObject,handles)

% --- Executes on button press in SteadyBox.
function SteadyBox_Callback(hObject, eventdata, handles)
% hObject    handle to SteadyBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.noiseTypes.steady = 1;
else
    handles.noiseTypes = rmfield(handles.noiseTypes,'steady');
end
guidata(hObject,handles)

% --- Executes on button press in AMSSNbox.
function AMSSNbox_Callback(hObject, eventdata, handles)
% hObject    handle to AMSSNbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.noiseTypes.AMSSN = 1;
else
    handles.noiseTypes = rmfield(handles.noiseTypes,'AMSSN');
end
guidata(hObject,handles)

% --- Executes on button press in SpeechShapedBox.
function SpeechShapedBox_Callback(hObject, eventdata, handles)
% hObject    handle to SpeechShapedBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.noiseTypes.speech = 1;
else
    handles.noiseTypes = rmfield(handles.noiseTypes,'speech');
end
guidata(hObject,handles)

% --- Executes on button press in BabbleBox.
function BabbleBox_Callback(hObject, eventdata, handles)
% hObject    handle to BabbleBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.noiseTypes.babble = 1;
else
    handles.noiseTypes = rmfield(handles.noiseTypes,'babble');
end
guidata(hObject,handles)

% --- Executes on button press in BothBox.
function BothBox_Callback(hObject, eventdata, handles)
% hObject    handle to BothBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.cueTypes.both = 1;
else
    handles.cueTypes = rmfield(handles.cueTypes,'both');
end
guidata(hObject,handles)

% --- Executes on button press in ITDBox.
function ITDBox_Callback(hObject, eventdata, handles)
% hObject    handle to ITDBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.cueTypes.ITD = 1;
else
    handles.cueTypes = rmfield(handles.cueTypes,'ITD');
end
guidata(hObject,handles)

% --- Executes on button press in IIDbox.
function IIDbox_Callback(hObject, eventdata, handles)
% hObject    handle to IIDbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.cueTypes.IID = 1;
else
    handles.cueTypes = rmfield(handles.cueTypes,'IID');
end
guidata(hObject,handles)

% --- Executes on button press in Location0.
function Location0_Callback(hObject, eventdata, handles)
% hObject    handle to Location0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.azimuths = [handles.azimuths 0];
else
    handles.azimuths = handles.azimuths(handles.azimuths ~= 0);
end
guidata(hObject,handles)

% --- Executes on button press in LocationRight.
function LocationRight_Callback(hObject, eventdata, handles)
% hObject    handle to LocationRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.azimuths = [handles.azimuths pi/2];
else
    handles.azimuths = handles.azimuths(handles.azimuths ~= pi/2);
end
guidata(hObject,handles)

% --- Executes on button press in LocationLeft.
function LocationLeft_Callback(hObject, eventdata, handles)
% hObject    handle to LocationLeft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject,'Value') == 1
    handles.azimuths = [handles.azimuths -pi/2];
else
    handles.azimuths = handles.azimuths(handles.azimuths ~= -pi/2);
end
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function SaveButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SaveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function LoadButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LoadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in resumeButton.
function resumeButton_Callback(hObject, eventdata, handles)
% hObject    handle to resumeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resume_inProgress('Run_in_Progress.mat');
uiresume(handles.figure1)
delete(handles.figure1);
