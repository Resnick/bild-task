function out = apply_Head_Shadow(source,Fs,angle)
%applyHeadShadow: This function applies head shadowing to the head-on
%binaural source based on the supplied source angle and Fs. The head shadow
%filter is generated using the functions for a spherical head presented in
%Brown et al. 1998.

% Author: Jesse Resnick, 2017.

c = 334; %speed of sound (m/s)
r = 0.089; % radius of head (m)
beta = 2*c/r;

%% 
% Define head shadow functions...see: 
% Phillip Brown, C., Duda, R.O., 1998. A structural model for binaural
% sound synthesis. IEEE Trans. Speech Audio Process. 6, 476�488.
% doi:10.1109/89.709673
HS_Lfunc = @(s,theta) ((1+cos(theta+pi/2)).*s+beta)./(s+beta);
HS_Rfunc = @(s,theta) ((1+cos(theta-pi/2)).*s+beta)./(s+beta);

samples = length(source);
k = 1:samples;
freqs = k/samples*Fs;

HS_Lfilter = HS_Lfunc(freqs,angle);
HS_Rfilter = HS_Rfunc(freqs,angle);

out = [source(:,1).*HS_Lfilter.', source(:,2).*HS_Rfilter.'];

end

