init_snr = 0;
temp_snr = 0.5*init_snr+51;
targetSNR = -20;


spondeeDir = 'AuxFiles';
steady_noise = audioread([spondeeDir,filesep,'noise.wav']);
[s1,fs] = audioread([spondeeDir,filesep,'birthday.wav']);    length_s1 = length(s1);
s1(:,2) = steady_noise(1:length_s1,1);
rawSignal = s1(:,1);
rawNoise = s1(:,2);
prevNoise = set_snr(targetSNR,s1);

rawSNR = 10*log10(var(rawSignal)/var(rawNoise));
prevSNR = 10*log10(var(rawSignal)/var(prevNoise));

