c = 334; %speed of sound
r = 0.089; % radius of head
beta = 2*c/r;
Fs = 44100;

input = audioread('playground.wav');
source = input(:,1);

%%
HS_Lfunc = @(s,theta) ((1+cos(theta+pi/2)).*s+beta)./(s+beta);
HS_Rfunc = @(s,theta) ((1+cos(theta-pi/2)).*s+beta)./(s+beta);

samples = length(source);
k = 1:samples;
freqs = k/samples*Fs;
angle = 0;
ITD = 700; %Not quite right but close enough.

HS_Lfilter = HS_Lfunc(freqs,angle);
HS_Rfilter = HS_Rfunc(freqs,angle);

plot(freqs,10*log10(HS_Lfilter)); hold on
plot(freqs,10*log10(HS_Rfilter))

%%
figure
source = input(:,1);
source_fft = fft(source);
samples = length(source);
nyquist = 0.5*Fs;
ITDpad = round(abs(ITD*1e-6*Fs));

if ITD < 0
    outL = [source.*HS_Lfilter.'; zeros(ITDpad,1)] ;
    outR = [zeros(ITDpad,1); source.*HS_Rfilter.'];
elseif ITD > 0
    outL = [zeros(ITDpad,1); source.*HS_Lfilter.'] ;
    outR = [source.*HS_Rfilter.';zeros(ITDpad,1)];
else
    outL = source.*HS_Lfilter.';
    outR = source.*HS_Rfilter.';
end

k2 = 1:samples+ITDpad;
freqs2 = k2/samples*Fs;

subplot(1,2,1); plot(k2/Fs,outR); ylim([-0.5,.5]); hold on;
plot(k/Fs,source); ylim([-0.5,.5]);
plot(k2/Fs,outL); ylim([-0.5,.5]);

subplot(1,2,2);
plot(freqs2,10*log10(abs(fft(outR)))); xlim([0,10000]); ylim([-20,35]); hold on
plot(freqs,10*log10(abs(source_fft))); xlim([0,10000]); ylim([-20,35])
plot(freqs2,10*log10(abs(fft(outL)))); xlim([0,10000]); ylim([-20,35])

sound([outL,outR],Fs)
filename='playground90L.wav';
audiowrite(filename,[outL,outR],Fs)
