

format LONGG
[s1,fs] = audioread('concatenate_spondee.wav');
ssn = audioread('SSN4.wav');
sjh = audioread('amnoise.wav');

lenssn = length(ssn);
lensjh = length(sjh);

int_snr = -42;
gain_number = 0;
ssn_divided_by = 100;

sSSN = [s1(1:lensjh)/ssn_divided_by,ssn(1:lensjh)];
sSJH = [s1(1:lensjh),sjh];

modssn = snr(int_snr,sSSN);
modsjh = snr(int_snr,sSJH);

pSSN = [s1(1:lensjh)/ssn_divided_by, modssn];
mpSSN = pSSN(:,1) + pSSN(:,2);

pSJH = [s1(1:lensjh), modsjh];
mpSJH = pSJH(:,1) + pSJH(:,2);
mpSJH = 0.01*mpSJH;
mpSJH = mpSJH*power(10, (gain_number/20));

% max(mpSSN)
a = max(mpSSN);
b = max(mpSJH);
fprintf('snr: %d\n', int_snr);
fprintf('gain number: %d\n', gain_number);
fprintf('max mpSSN: %d\n', a);
fprintf('max mpSJH: %d\n', b);
fprintf('\n');