spondeeDir = '../AuxFiles';
[s1,fs] = audioread([spondeeDir,filesep,'birthday.wav']);    length_s1 = length(s1);
s2 = audioread([spondeeDir,filesep,'drawbridge.wav']);       length_s2 = length(s2);
s3 = audioread([spondeeDir,filesep,'eardrum.wav']);          length_s3 = length(s3);
s4 = audioread([spondeeDir,filesep,'iceberg.wav']);          length_s4 = length(s4);
s5 = audioread([spondeeDir,filesep,'mousetrap.wav']);        length_s5 = length(s5);
s6 = audioread([spondeeDir,filesep,'northwest.wav']);        length_s6 = length(s6);
s7 = audioread([spondeeDir,filesep,'padlock.wav']);          length_s7 = length(s7);
s8 = audioread([spondeeDir,filesep,'playground.wav']);       length_s8 = length(s8);
s9 = audioread([spondeeDir,filesep,'sidewalk.wav']);         length_s9 = length(s9);
s10 = audioread([spondeeDir,filesep,'stairway.wav']);        length_s10 = length(s10);
s11 = audioread([spondeeDir,filesep,'toothbrush.wav']);      length_s11 = length(s11);
s12 = audioread([spondeeDir,filesep,'woodwork.wav']);        length_s12 = length(s12);
calib = audioread([spondeeDir,filesep,'calib_tone.wav']);

calibRMS = std(calib);

for i = 1:12
    eval(sprintf('s(1:length(s%d),%d) = s%d(:,1);',i,i,i));
    firstIDX = find(s(:,i) ~= 0,1,'first');
    lastIDX  = find(s(:,i) ~= 0,1,'last');
    totalRMS(i) = std(s(:,i));
    signalRMS(i) = std(s(firstIDX:lastIDX,i));
end