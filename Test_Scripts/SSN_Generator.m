clear all; close all; clc

[s1,fs] = audioread('1 Audio Track.aiff');
s2 = audioread('2 Audio Track.aiff');
s_original = audioread('amnoise.wav');

% Find the non-zero entries, to get rid of the long pulse between each phrase
[row1,col1] = find(s1(:,2));
[row2,col2] = find(s2(:,2));

% Rebuild the HINT sentences
s1_new = [s1(row1,2),s1(row1,2)];
s2_new = [s2(row2,2),s2(row2,2)];

% Add two HINT sentences together to get the speech noise
len1 = min(length(row1),length(row2));
speech_sig = s1_new(1:len1,:) + s2_new(1:len1,:);

% Take the Hilbert Envelop of the speech signal
hil_env = abs(hilbert(speech_sig)); % Two columns are the same


%%%%% The second part: To construct the speech shaped noise (SSN)

% Construct the filter for the noise
% The numbers are from the Byrne.D paper
fre = [0 10 30 40 50 80 100 125 160 200 250 315 400 500 630 800 1000 1250 1600 2000 ... 
       2500 3150 4000 5000 6300 8000 10000 12500 16000];
magn = [-inf -inf -inf -inf -inf 43.5 54.4 57.7 56.8 60.2 60.3 59.0 62.1 62.1 60.5 56.8 ...
        53.7 53.0 52.0 48.7 48.1 46.8 45.6 44.5 44.3 43.7 43.4 41.3 40.7];
real_magn = 10.^(magn/20); % Scale it back from dB to real magnitude

% This step builds the FIR filter with order of 255, which will give 256
% points. Then, the filter is divided by 500 to scale down. This is because
% in low pass filter (which is similar in this case), there will be a gain
% of the filter, so to output signal will have a gain. To reduce the gain,
% we divided this filter by the sum of the filter coefficients. 500 is
% decided empirically. 
b = fir2(512,fre/16000,real_magn)/500;
[H,W] = freqz(b,1); % Acquire the coefficients of the filter
wf = fre/16000 * pi; % Normalize the frequency to from 0 to pi for plotting

%%% Plot the calculated filter against the original filter
figure
stem(W,abs(H)) % Calculated filter
hold on
stem(wf, abs(real_magn)/500,'r') % Original filter
hold off
legend('Calculated Filter','Original Filter')

% Construct the random noise and apply the filter to it
N = 8192; % Used for plots. We get N points from fft function
noise_fft = zeros(N,2);
noise_filtered_store = zeros(length(speech_sig),1);
% We filter the noise 160 times to make sure the signal has the proper
% spectrum shape 
for i = 1:200    
    noise = randn(length(speech_sig),1);
    noise_filtered = filter(b,1,noise);
    noise_fft = noise_fft + [abs(fft(noise,N)), abs(fft(noise_filtered,N))];
    % Add all the filtered signals in freq domain together
    noise_filtered_store = noise_filtered + noise_filtered_store;
end

nf_db = db(noise_fft); % Change to dB scale
% Scale the dB signal so the max point matches with the one from the paper
nf_db = nf_db + max(magn) - max(nf_db(:,2));

figure
semilogx((0:N-1)*32000/N, nf_db)
axis tight
xlim([10,16000])
legend('Noise','SSN')

%%%%% The third part: Apply the Hilbert Envelop to the SSN

modnoise = noise_filtered.* hil_env(:,1);
% Average the SSN
noise_filtered_store = noise_filtered_store./160;
modnoise2 = noise_filtered_store.* hil_env(:,1);
modnoise3 = abs(sqrt(var(s_original)/var(modnoise2))) * modnoise2;
% modnoise3 is the noise with same var as the original amnoise created by
% Zheng Hao


% audiowrite('SSN1.wav',modnoise,fs);
% audiowrite('SSN2.wav',modnoise2,fs);
% audiowrite('SSN3.wav',modnoise3,fs');
%%

% This part is to find out how long the silence is before and after the
% spondee in each sound file. 

clear all; close all; clc

% Load all the files and find the length of each of them
[s1,fs] = audioread('birthday.wav');
length_s1 = length(s1);
s2 = audioread('drawbridge.wav');
length_s2 = length(s2);
s3 = audioread('eardrum.wav');
length_s3 = length(s3);
s4 = audioread('iceberg.wav');
length_s4 = length(s4);
s5 = audioread('mousetrap.wav');
length_s5 = length(s5);
s6 = audioread('northwest.wav');
length_s6 = length(s6);
s7 = audioread('padlock.wav');
length_s7 = length(s7);
s8 = audioread('playground.wav');
length_s8 = length(s8);
s9 = audioread('sidewalk.wav');
length_s9 = length(s9);
s10 = audioread('stairway.wav');
length_s10 = length(s10);
s11 = audioread('toothbrush.wav');
length_s11 = length(s11);
s12 = audioread('woodwork.wav');
length_s12 = length(s12);

a1 = logical(s1(:,1));
a2 = logical(s2(:,1));
a3 = logical(s3(:,1));
a4 = logical(s4(:,1));
a5 = logical(s5(:,1));
a6 = logical(s6(:,1));
a7 = logical(s7(:,1));
a8 = logical(s8(:,1));
a9 = logical(s9(:,1));
a10 = logical(s10(:,1));
a11 = logical(s11(:,1));
a12 = logical(s12(:,1));

con_Spondee = [s1(a1,1)
               s2(a2,1)
               s3(a3,1)
               s4(a4,1)
               s5(a5,1)
               s6(a6,1)
               s7(a7,1)
               s8(a8,1)
               s9(a9,1)
               s10(a10,1)
               s11(a11,1)
               s12(a12,1)];

audiowrite('concatenate_Spondee.wav', con_Spondee, fs);









