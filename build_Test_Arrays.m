function [testIDX,testMat,testCell] = build_Test_Arrays(azimuths,cueTypes,noiseTypes)
%build_Test_Arrays This function resturns the indices of tests to perform
%(testIDX) that index into a constructed a binary array to hold test
%statuses (testMat) and a cell array of options (testCell).

% Extract names from structures created. Might want to make these cell
% arrays to begin with at some point.
cueTypeNames = fieldnames(cueTypes);
noiseTypeNames = fieldnames(noiseTypes);

% Find indices for no delay and delay trials since the former doesn't need
% to be tested with multiple cueTypes.
azimuths = sort(azimuths);
NoDelay = find(azimuths == 0);
Delay = find(azimuths ~= 0);


% Initialize arrays
testMat = zeros(length(azimuths),length(cueTypeNames),length(noiseTypeNames));
testCell = num2cell(testMat);

% Populate no delay expt types.
if NoDelay
    for noiseIDX = 1:length(noiseTypeNames)
        thisTestOpts = {azimuths(NoDelay),cueTypeNames{1},noiseTypeNames{noiseIDX}};
        testCell{NoDelay,1,noiseIDX} = thisTestOpts;
        testMat(NoDelay,1,noiseIDX) = 1;
    end
end

% Populate delay expt types.
for delayIDX = Delay
    for noiseIDX = 1:length(noiseTypeNames)
        for cueIDX = 1:length(cueTypeNames)
            thisTestOpts = {azimuths(delayIDX),cueTypeNames{cueIDX},noiseTypeNames{noiseIDX}};
            testCell{delayIDX,cueIDX,noiseIDX} = thisTestOpts;
            testMat(delayIDX,cueIDX,noiseIDX) = 1;
        end
    end
end
testIDX = find(testMat == 1);

end

