function result = play_BILD(startingSnr,speechLevel,angle,cueType,noiseType,subjectName)
% play_BILD_JR: This function performs one BILD experiment with the
% parameters specified. Cue and noise types are specified using the flags:
%       cueType- 'ITD', 'IID', 'both'       % May want conflicting cues too
%       noiseType-- 'steady','AMSSN','speech','babble'

p = inputParser;
validCues = {'ITD','IID','both'};
validNoise = {'steady','AMSSN','speech','babble'};
addRequired(p,'startingSnr',@(x) x>-30 && x<50) %check limits
addRequired(p,'speechLevel',@(x) x > 30 && x < 80) %check limits
addRequired(p,'angle',@(x) abs(x)<= pi/2)
addRequired(p,'cueType',@(x) any(strcmp(x,validCues)));
addRequired(p,'noiseType',@(x) any(strcmp(x,validNoise)));
addRequired(p,'subjectName',@(x) ischar(x))
parse(p,startingSnr,speechLevel,angle,cueType,noiseType,subjectName)

format long
beep off

numRev = 14; % numer of reversals
revInt = 10; % reversals to be averaged in result

% Pre-allocate reversal SNR holders
before_rev = nan(numRev-revInt,1);
rev = nan(revInt,1);

time = fix(clock); today = date;
hour = num2str(time(4)); minute = num2str(time(5),'%02d');
datadir = ['.', filesep,'Data',filesep,subjectName,filesep,today,filesep];
if ~isdir(datadir); mkdir(datadir); end %#ok<ISDIR>
if angle ~= 0
    taskname = sprintf('%s_%s_%+03d_',noiseType,cueType,round(angle*90/(pi/2)));
else
    taskname = sprintf('%s_None_%+03d_',noiseType,round(angle*90/(pi/2)));
end
txtfullname = [datadir,taskname, hour,'_', minute,'_',subjectName,'.txt'];
fid = fopen(txtfullname,'w');
fprintf(fid,'\r\n');
fprintf(fid,'*Subject name: %s \r\n', subjectName);
fprintf(fid,'*Date: %s, Time: %s:%s \r\n', today, hour, minute);
fprintf(fid,'Noise Type: %s \n',noiseType);
fprintf(fid,'Cue Type: %s \n',cueType);
fprintf(fid,'Source Location: %d \n',round(angle*90/(pi/2)));
fprintf(fid,'\r\n');
fprintf(fid,'LEVEL          Correct or Not   \r\n');

%go(1,1) is for the case of -100dB SNR condtion
%go(76,1) is for the case of +50dB SNR condition
%Equation: x dB = 2*go(i,1) - 102;

go = zeros(76,1); count = zeros(76,1);
spondeeDir = 'AuxFiles';
switch noiseType
    case 'steady'
        rawNoise = audioread([spondeeDir,filesep,'noise.wav']);
    case 'AMSSN'
        error('Amplitude modulated noise not yet implemented.');
    case 'speech'
        warning('Speech-shaped noise needs to be refined.');
        rawNoise = audioread([spondeeDir,filesep,'SSN4.wav']);
    case 'babble'
        error('Babble noise nnot yet implemented.');
end

%% Read in audio files and define length;

[temp,fs] = audioread([spondeeDir,filesep,'birthday.wav']);
spondee{1} = temp(:,1);   lengthS(1) = length(temp);
temp = audioread([spondeeDir,filesep,'drawbridge.wav']);
spondee{2} = temp(:,1);   lengthS(2) = length(temp);
temp = audioread([spondeeDir,filesep,'eardrum.wav']);
spondee{3} = temp(:,1);   lengthS(3) = length(temp);
temp =  audioread([spondeeDir,filesep,'iceberg.wav']);
spondee{4} = temp(:,1);   lengthS(4) = length(temp);
temp = audioread([spondeeDir,filesep,'mousetrap.wav']);
spondee{5} = temp(:,1);   lengthS(5) = length(temp);
temp = audioread([spondeeDir,filesep,'northwest.wav']);
spondee{6} = temp(:,1);   lengthS(6) = length(temp);
temp = audioread([spondeeDir,filesep,'padlock.wav']);
spondee{7} = temp(:,1);   lengthS(7) = length(temp);
temp = audioread([spondeeDir,filesep,'playground.wav']);
spondee{8} = temp(:,1);   lengthS(8) = length(temp);
temp = audioread([spondeeDir,filesep,'sidewalk.wav']);
spondee{9} = temp(:,1);   lengthS(9) = length(temp);
temp = audioread([spondeeDir,filesep,'stairway.wav']);
spondee{10} = temp(:,1);   lengthS(10) = length(temp);
temp = audioread([spondeeDir,filesep,'toothbrush.wav']);
spondee{11} = temp(:,1);   lengthS(11) = length(temp);
temp = audioread([spondeeDir,filesep,'woodwork.wav']);
spondee{12} = temp(:,1);   lengthS(12) = length(temp);
calib = audioread([spondeeDir,filesep,'calib_tone.wav']);
for sigIDX=1:12
    level(sigIDX) = std(spondee{sigIDX});
end
meanLevel = mean(level);

% Calculate the factor for setting speech level
ampConstant = 8.03737; % Empirically determined via sound level meter. JR 4/9/2018
speechSPLconstant = 10^(speechLevel/20)*20E-6/(ampConstant*std(calib));

% Set angle and number of samples to pad either side for ITD.
if any(strcmp(cueType,{'ITD','both'}))
    ITD = 700*angle/(pi/2); % in us
    ITDpad = round(ITD*1e-6*fs); %700 us * sample_rate
    z = zeros(abs(ITDpad),1);
end

% Define noise attributes
rampDur = round(50*1e-3*fs); % noise ramp duration in samples
rampOn = 1:rampDur;

%% Start a new trial
disp('new run');
init_snr = startingSnr;
reversenumber = 0;  firstTrial = 1;    correct = nan;
[handles,hObject] = response();

while reversenumber < numRev
    num = randi(12,1);
    temp_snr = 0.5*init_snr + 51;
    go(temp_snr,1) = go(temp_snr,1) + 1;
    
    % Select spondee
    signal = spondee{num}; exactanswer = num;
    signal = signal * meanLevel/level(num)*speechSPLconstant;
    
    %% Create appropriate length noise.
    noiseRand = randi(lengthS(num));
    noise = rawNoise(noiseRand:noiseRand+lengthS(num)-1);
    
    % Produce stimulus
    noise = set_snr(init_snr, signal,noise);
    ramp = ones(length(signal),1);
    ramp(1:rampDur) = (rampOn-1)/(rampDur-1);
    ramp(end-rampDur+1:end) = 1-((rampOn-1)/(rampDur-1));
    noise = noise .* ramp;
    lSpondee = signal(:,1); rSpondee = signal(:,1);
    l_mask = noise;
    r_mask = noise;
    if any(strcmp(cueType,{'IID','both'}))
        % Calculate head shadow based on spherical head.
        shadowOut = apply_Head_Shadow([lSpondee,rSpondee],fs,angle);
        lSpondee = shadowOut(:,1); rSpondee =shadowOut(:,2);
        %             % Normalize loudness of best ear. Note: need to select signals
        %             % > 1e6 to prevent error in mean introduced by rounding in
        %             % filter application.
        %             rawLoudness = std(signal(:,1));
        %             if std(lSpondee) > std(rSpondee)
        %                 shadowLoudness = std(lSpondee);
        %             else
        %                 shadowLoudness = std(rSpondee);
        %             end
        %             shadowGain = shadowLoudness/rawLoudness;
        %             lSpondee = lSpondee/shadowGain; rSpondee = rSpondee/shadowGain;
    end
    if any(strcmp(cueType,{'ITD','both'}))
        if ITD > 0 && abs(ITD) <= 800
            lSpondee = [z;lSpondee]; %#ok<*AGROW>
            rSpondee = [rSpondee;z];
            l_mask = [noise;z];
            r_mask = [noise;z];
        elseif ITD < 0 && abs(ITD) <= 800
            rSpondee = [z;rSpondee];
            lSpondee = [lSpondee;z];
            l_mask = [noise;z];
            r_mask = [noise;z];
        elseif ITD ~= 0
            error('Non-physiologic ITD: %f',ITD);
        end
    end
    r_mono = rSpondee+ r_mask;
    l_mono = lSpondee+ l_mask;
    mp = [l_mono, r_mono];
    if max(mp) > 1
        fprintf('Peak clipping occured. SNR %d:',init_snr);
    end
    stimDuration = length(mp)/fs;
    sound(mp,fs);
    pause(stimDuration);
    
    % User response
    try
        handles = response('resume_Callback',hObject,[],handles);
        user_response = handles.output;
    catch
        fprintf('GUI closed before trial completion.');
        user_response = 'abort';
    end
    if strcmp(user_response,'abort')
        result = 'abort';
        return
    end
    if strfind(user_response,'Birthday'); givinganswer = 1; %#ok<*STRIFCND>
    elseif strfind(user_response,'Drawbridge'); givinganswer = 2;
    elseif strfind(user_response,'Eardrum'); givinganswer = 3;
    elseif strfind(user_response,'Iceberg'); givinganswer = 4;
    elseif strfind(user_response,'Mousetrap'); givinganswer = 5;
    elseif strfind(user_response,'Northwest'); givinganswer = 6;
    elseif strfind(user_response,'Padlock'); givinganswer = 7;
    elseif strfind(user_response,'Playground'); givinganswer = 8;
    elseif strfind(user_response,'Sidewalk'); givinganswer = 9;
    elseif strfind(user_response,'Stairway'); givinganswer = 10;
    elseif strfind(user_response,'Toothbrush'); givinganswer = 11;
    elseif strfind(user_response,'Woodwork'); givinganswer = 12;
    else; error('Unknown response class: %s',user_response);
    end %end of switch sentence
    
    % Update SNR
    if exactanswer == givinganswer
        if firstTrial == 1
            firstTrial = 0;
            temp_count = 0.5*init_snr +51;
            count(temp_count,1) = count(temp_count,1) + 1;
        else
            if correct == 1
                temp_count = 0.5*init_snr +51;
                count(temp_count,1) = count(temp_count,1) + 1;
            else
                reversenumber = reversenumber + 1;
                temp_count = 0.5*init_snr +51;
                count(temp_count,1) = count(temp_count,1) + 1;
                
                switch reversenumber
                    case num2cell(1:numRev-revInt)
                        before_rev(reversenumber) = init_snr;
                    case num2cell(numRev-revInt+1:numRev)
                        rev(reversenumber-(numRev-revInt)) = init_snr;
                end %end of switch reverenumber sentence
            end
        end
        correct = 1;
        fprintf(fid,'%d                   1          \r\n', init_snr);
        if reversenumber < 2
            init_snr = init_snr - 4;
        else
            init_snr = init_snr - 2;
        end
        
        if init_snr > 50
            init_snr = 50; warning('Maximum SNR reached (SNR = +50).');
        end
        
        if init_snr < -100
            init_snr = -100; warning('Minimum SNR reached (SNR = -100).');
        end        
    else %givinganswer != exactanswer
        if firstTrial == 1; firstTrial = 0;
        else
            if correct == 1
                reversenumber = reversenumber +1;
                switch reversenumber
                    case num2cell(1:numRev-revInt)
                        before_rev(reversenumber) = init_snr;
                    case num2cell(numRev-revInt+1:numRev)
                        rev(reversenumber-(numRev-revInt)) = init_snr;
                end %end of switch reverenumber sentence
            end
        end
        correct = 0;
        fprintf(fid,'%d                   0          \r\n', init_snr);
        init_snr = init_snr + 2;
        if init_snr > 50
            init_snr = 50;
        end
        
        if init_snr < -100
            init_snr = -100;
        end
    end %end of if sentence
end %end of while sentence
delete(handles.figure1);
clear('hObject','handles');

result.total = [before_rev;rev]; %for plotting
result.rev = rev; %for calculating
result.mean = mean(rev);
result.std = std(rev);
result.go = go;
result.count = count;
fclose(fid);
end

