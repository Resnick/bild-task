function [output] = hsfilter_sphere(theta,Fs,input)
% filters in input signal according to head shadowing using a rigid
% spherical model of the head. Theta is the angle with the frontal plane in
% degrees. Fs is the sample rate.
%

theta  = theta + 90;
theta0 = 150; alpha_min=0.05;
c=334; %speed of sound
a=0.08; %radius of head
w0 = c/a;
alpha = alpha_min + (1-alpha_min/2)*cos(theta/theta0*pi);

% numerator of Transfer function
B = [(alpha+w0/Fs)/(1+w0/Fs), (-alpha+w0/Fs)/(1+w0/Fs)];

% denominator of Transfer function
A = [1, -(1-w0/Fs)/(1+w0/Fs)];

if (abs(theta) < 90)
    gdelay = -Fs/w0*(cos(theta*pi/180)-1);
else
    gdelay = Fs/w0*((abs(theta)-90)*pi/180+1);
end
a = (1-gdelay/1+gdelay); % allpass filter coefficient
out_magn = filter(B,A,input);
output = filter([a,1],[1,a],out_magn);
end