function play_Selected(subjectName,initSnr,numRepetition,speechLevel,...
     noiseTypes, azimuths, cueTypes)
% PLAY_SELECTED accepts information about the subject and the desired
% experiments to run and then runs through all these experiments in random
% order for each numRepetition.


% Package the different selected options into nice arrays. testCell
% contains the options for the tests to be performed, testMat is a binary
% matrix indicating whether test still need to be done, and testIDX
% contains the indices of tests to perform
[testIDX,testMat,testCell] = build_Test_Arrays(azimuths,cueTypes,noiseTypes);


for i = 1:numRepetition %(EX)[1 3 5, 5 1 3, 3 1 5, 3 5 1]
    randIDX = testIDX(randperm(length(testIDX))); %(EX) 3 1 2
    tempMat = testMat;
    for j = 1:length(randIDX)
        tempTest = randIDX(j);
        tempOptions = testCell{tempTest};
        result = play_BILD(initSnr,speechLevel,tempOptions{:},subjectName);
        if strcmp(result,'abort')
            if exist('results','var')
                % Save everything except trial in progres to resume if desired.
                iLast = i; jLast = j;
                save('Run_in_Progress.mat','results','testIDX','testMat',...
                    'testCell','randIDX','tempTest','numRepetition',...
                    'iLast','jLast','initSnr','speechLevel','subjectName')
                fprintf('Trial aborted. Data saved. \n')
            else
                fprintf('Trial aborted. No completed trials to save. \n')
            end
            return
        else
            results(tempTest,i) = result; %#ok<AGROW>
            tempMat(tempTest) = 0;
            if j < length(randIDX)
                continue_Prompt()
            end
        end
    end
    if i ~= numRepetition
        take_rest('Title', 'Take a rest?');
    end
end

for j = 1:length(testIDX)
    writing_result(testCell{testIDX(j)}, numRepetition, results(testIDX(j),:), subjectName);
end


