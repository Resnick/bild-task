function snredNoise = set_snr(snr, spondee,noise)
%s: targeted snr value, x: wavread('***.wav')

format long

stdSpondee = std(spondee); % Find the standard deviation of the spondee
stdNoise = std(noise); % Find the standard deviation of the noise.

stdNewNoise = stdSpondee /(10^(snr/20));
% This equation is derived from snrDB = 20 * log10[rms(spondee)/rms(noise)]

mul = abs(stdNewNoise/stdNoise); % Find the real part

snredNoise = mul*noise; % Scale the noise signal so it has the desired std, which corresponds to the desired snr