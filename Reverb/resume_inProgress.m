function resume_inProgress(lastRunData)
%RESUME_INPROGRESS This function loads the in progress results from and
%aborted play_Selected run and resumes the experiment from there.

% lastRunData should be a '.mat' file containing: 'results', 'testIDX',
% 'testMat', 'testCell', 'randIDX', 'tempTest', 'numRepetition', 'i', 'j',
% 'init_snr', 'gain_number', 'subjectName'
load(lastRunData)

for i = iLast:numRepetition %(EX)[1 3 5, 5 1 3, 3 1 5, 3 5 1]
    if i > iLast
        randIDX = testIDX(randperm(length(testIDX))); %(EX) 3 1 2
        tempMat = testMat;
        jmin = 1;
    else
        jmin = jLast;
    end
    for j = jmin:length(randIDX)
        tempTest = randIDX(j);
        tempOptions = testCell{tempTest};
        result = play_BILD(initSnr,speechLevel,tempOptions{:},subjectName);
        if strcmp(result,'abort')
            iLast = i; jLast = j;
            % Save everything except trial in progres to resume if desired.
            save('Run_in_Progress.mat','results','testIDX',...
                'testMat','testCell','randIDX','tempTest','numRepetition',...
                'iLast','jLast','initSnr','speechLevel','subjectName')
            fprintf('Trial aborted. Data saved.')
        else
            results(tempTest,i) = result; %#ok<AGROW>
            tempMat(tempTest) = 0;
            if j < length(randIDX)
                continue_Prompt()
            end
        end
    end
    if i ~= numRepetition
        take_rest('Title', 'Take a rest?');
    end
end

for j = 1:length(testIDX)
    writing_result(testCell{testIDX(j)}, numRepetition, results(testIDX(j),:), subjectName);
end
end
