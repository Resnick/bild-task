function snredNoise = set_snr_Reverb(snr, spondee,noise)
%s: targeted snr value, x: wavread('***.wav')

format long

truncatedSpondee = spondee(20000:70000,1);
startpad = zeros(19999,1);
endpad = zeros(length(spondee)-7.0e4,1);
spondee = [startpad;truncatedSpondee;endpad];

stdSpondee = mean(std(spondee)); % Find the standard deviation of the spondee
stdNoise = mean(std(noise)); % Find the standard deviation of the noise.

stdNewNoise = stdSpondee /(10^(snr/20));
% This equation is derived from snrDB = 20 * log10[rms(spondee)/rms(noise)]

mul = abs(stdNewNoise/stdNoise); % Find the real part

snredNoise = mul*noise; % Scale the noise signal so it has the desired std, which corresponds to the desired snr