function play_Selected_reverb(subjectName,initSnr,numRepetition,speechLevel,...
     roomTypes)
% PLAY_SELECTED accepts information about the subject and the desired
% experiments to run and then runs through all these experiments in random
% order for each numRepetition.

testMat = ones(length(roomTypes));

for i = 1:numRepetition %(EX)[1 3 5, 5 1 3, 3 1 5, 3 5 1]
    testIDX = randperm(length(roomTypes));
    randRooms = roomTypes(testIDX); %(EX) 3 1 2
    tempMat = testMat;
    for j = 1:length(testIDX)
        tempRoom = randRooms{j};
        result = play_revSRT(initSnr,speechLevel,tempRoom,subjectName);
        if strcmp(result,'abort')
            if exist('results','var')
                % Save everything except trial in progres to resume if desired.
                iLast = i; jLast = j;
                save('Run_in_Progress.mat','results','tempRoom',...
                    'numRepetition','iLast','jLast','initSnr',...
                    'speechLevel','subjectName')
                fprintf('Trial aborted. Data saved. \n')
            else
                fprintf('Trial aborted. No completed trials to save. \n')
            end
            return
        else
            results(testIDX(j),i) = result; %#ok<AGROW>
            tempMat(testIDX(j)) = 0;
            if j < length(testIDX)
                continue_Prompt()
            end
        end
    end
    if i ~= numRepetition
        take_rest('Title', 'Take a rest?');
    end
end

for j = 1:length(testIDX)
    writing_result_Reverb(roomTypes{testIDX(j)}, numRepetition, results(testIDX(j),:), subjectName);
end


