function result = play_revSRT(startingSnr,speechLevel,roomType,subjectName)
% play_revSRT: This function performs one reverberant signal in noise experiment with the
% parameters specified. roomType allows selection of the room used, current
% options:
%       'anechoic' - anechoic chamber
%       'meeting' - meeting room
%       'classroom' - classroom

p = inputParser;
validRooms = {'anechoic','meeting','auditorium'};
addRequired(p,'startingSnr',@(x) x>-30 && x<60) %check limits
addRequired(p,'speechLevel',@(x) x > 30 && x < 60) %check limits
addRequired(p,'roomType',@(x) any(strcmp(x,validRooms)));
addRequired(p,'subjectName',@(x) ischar(x))
parse(p,startingSnr,speechLevel,roomType,subjectName)

format long
beep off

numRev = 14; % numer of reversals
revInt = 10; % reversals to be averaged in result

% Pre-allocate reversal SNR holders
before_rev = nan(numRev-revInt,1);
rev = nan(revInt,1);

time = fix(clock); today = date;
hour = num2str(time(4)); minute = num2str(time(5),'%02d');
datadir = ['.',filesep,'Data',filesep,subjectName,filesep,today,filesep];
if ~isdir(datadir); mkdir(datadir); end %#ok<ISDIR>
taskname = sprintf('%s_',roomType);
txtfullname = [datadir,taskname, hour,'_', minute,'_',subjectName,'.txt'];
fid = fopen(txtfullname,'w');
fprintf(fid,'*Subject name: %s \r\n', subjectName);
fprintf(fid,'*Date: %s, Time: %s:%s \r\n', today, hour, minute);
fprintf(fid,'*Reverberant Environment: %s \r\n', roomType);
fprintf(fid,'\r\n');
fprintf(fid,'LEVEL          Correct or Not   \r\n');

%go(1,1) is for the case of -100dB SNR condtion
%go(76,1) is for the case of +50dB SNR condition
%Equation: x dB = 2*go(i,1) - 102;

go = zeros(76,1); count = zeros(76,1);
spondeeDir = 'Reverb_Aux';
rawNoise = audioread([spondeeDir,filesep,'noise_',roomType,'.wav']);

%% Read in audio files and define length;

[temp,fs] = audioread([spondeeDir,filesep,'birthday_',roomType,'.wav']);
spondee{1} = temp;   lengthS(1) = length(temp);
temp = audioread([spondeeDir,filesep,'drawbridge_',roomType,'.wav']);
spondee{2} = temp;   lengthS(2) = length(temp);
temp = audioread([spondeeDir,filesep,'eardrum_',roomType,'.wav']);
spondee{3} = temp;   lengthS(3) = length(temp);
temp =  audioread([spondeeDir,filesep,'iceberg_',roomType,'.wav']);
spondee{4} = temp;   lengthS(4) = length(temp);
temp = audioread([spondeeDir,filesep,'mousetrap_',roomType,'.wav']);
spondee{5} = temp;   lengthS(5) = length(temp);
temp = audioread([spondeeDir,filesep,'northwest_',roomType,'.wav']);
spondee{6} = temp;   lengthS(6) = length(temp);
temp = audioread([spondeeDir,filesep,'padlock_',roomType,'.wav']);
spondee{7} = temp(:,1);   lengthS(7) = length(temp);
temp = audioread([spondeeDir,filesep,'playground_',roomType,'.wav']);
spondee{8} = temp(:,1);   lengthS(8) = length(temp);
temp = audioread([spondeeDir,filesep,'sidewalk_',roomType,'.wav']);
spondee{9} = temp(:,1);   lengthS(9) = length(temp);
temp = audioread([spondeeDir,filesep,'stairway_',roomType,'.wav']);
spondee{10} = temp(:,1);   lengthS(10) = length(temp);
temp = audioread([spondeeDir,filesep,'toothbrush_',roomType,'.wav']);
spondee{11} = temp(:,1);   lengthS(11) = length(temp);
temp = audioread([spondeeDir,filesep,'woodwork_',roomType,'.wav']);
spondee{12} = temp(:,1);   lengthS(12) = length(temp);
calib = audioread([spondeeDir,filesep,'calib_tone.wav']);
for sigIDX=1:12
    level(sigIDX) = mean(std(spondee{sigIDX}));
end
meanLevel = mean(level);

% Calculate the factor for setting speech level
ampConstant = 8.03737; % Empirically determined via sound level meter. JR 4/9/2018
switch roomType % Conversion factors required due to HRTF changing loudness.
    case 'anechoic'
        ampConstant = ampConstant/0.4712;
    case 'meeting'
        ampConstant = ampConstant/0.3918;
    case 'auditorium'
        ampConstant = ampConstant/0.3342;
end
speechSPLconstant = 10^(speechLevel/20)*20E-6/(ampConstant*std(calib));

% Define noise attributes
rampDur = round(10*1e-3*fs); % noise ramp duration in samples
rampOn = 1:rampDur;

%% Start a new trial
disp('new run');
init_snr = startingSnr;
reversenumber = 0;  firstTrial = 1;    correct = nan;
[handles,hObject] = response();

while reversenumber < numRev
    num = randi(12,1);
    temp_snr = 0.5*init_snr + 51;
    go(temp_snr,1) = go(temp_snr,1) + 1;
    
    % Select spondee
    signal = spondee{num}; exactanswer = num;
    signal = signal * meanLevel/level(num)*speechSPLconstant;
    
    %% Create appropriate length noise.
    noiseRand = randi(lengthS(num));
    noise = rawNoise(noiseRand:noiseRand+lengthS(num)-1,:);
    
    % Produce stimulus
    noise = set_snr_Reverb(init_snr, signal,noise);
    ramp = ones(length(signal),1);
    ramp(1:rampDur) = (rampOn-1)/(rampDur-1);
    ramp(end-rampDur+1:end) = 1-((rampOn-1)/(rampDur-1));
    noise(:,1) = noise(:,1) .* ramp;
    noise(:,2) = noise(:,2) .* ramp;
    lSpondee = signal(:,1); rSpondee = signal(:,1);
    l_mask = noise(:,1);
    r_mask = noise(:,2);
    r_mono = rSpondee + r_mask;
    l_mono = lSpondee + l_mask;
    mp = [l_mono, r_mono];
    if max(mp) > 1
        fprintf('Peak clipping occured. SNR %d:',init_snr);
    end
    stimDuration = length(mp)/fs;
    sound(mp,fs);
    pause(stimDuration);
    
    % User response
    try
        handles = response('resume_Callback',hObject,[],handles);
        user_response = handles.output;
    catch
        fprintf('GUI closed before trial completion.');
        user_response = 'abort';
    end
    if strcmp(user_response,'abort')
        result = 'abort';
        return
    end
    if strfind(user_response,'Birthday'); givinganswer = 1; %#ok<*STRIFCND>
    elseif strfind(user_response,'Drawbridge'); givinganswer = 2;
    elseif strfind(user_response,'Eardrum'); givinganswer = 3;
    elseif strfind(user_response,'Iceberg'); givinganswer = 4;
    elseif strfind(user_response,'Mousetrap'); givinganswer = 5;
    elseif strfind(user_response,'Northwest'); givinganswer = 6;
    elseif strfind(user_response,'Padlock'); givinganswer = 7;
    elseif strfind(user_response,'Playground'); givinganswer = 8;
    elseif strfind(user_response,'Sidewalk'); givinganswer = 9;
    elseif strfind(user_response,'Stairway'); givinganswer = 10;
    elseif strfind(user_response,'Toothbrush'); givinganswer = 11;
    elseif strfind(user_response,'Woodwork'); givinganswer = 12;
    else; error('Unknown response class: %s',user_response);
    end %end of switch sentence
    
    % Update SNR
    if exactanswer == givinganswer
        if firstTrial == 1
            firstTrial = 0;
            temp_count = 0.5*init_snr +51;
            count(temp_count,1) = count(temp_count,1) + 1;
        else
            if correct == 1
                temp_count = 0.5*init_snr +51;
                count(temp_count,1) = count(temp_count,1) + 1;
            else
                reversenumber = reversenumber + 1;
                temp_count = 0.5*init_snr +51;
                count(temp_count,1) = count(temp_count,1) + 1;
                
                switch reversenumber
                    case num2cell(1:numRev-revInt)
                        before_rev(reversenumber) = init_snr;
                    case num2cell(numRev-revInt+1:numRev)
                        rev(reversenumber-(numRev-revInt)) = init_snr;
                end %end of switch reverenumber sentence
            end
        end
        correct = 1;
        
        fprintf(fid,'%d                   1          \r\n', init_snr);
        if reversenumber < 2
            init_snr = init_snr - 4;
        else
            init_snr = init_snr - 2;
        end
        
        if init_snr > 50
            init_snr = 50; warning('Maximum SNR reached (SNR = +50).');
        end
        
        if init_snr < -100
            init_snr = -100; warning('Minimum SNR reached (SNR = -100).');
        end
        
    else %givinganswer != exactanswer
        if firstTrial == 1; firstTrial = 0;
        else
            if correct == 1
                reversenumber = reversenumber +1;
                switch reversenumber
                    case num2cell(1:numRev-revInt)
                        before_rev(reversenumber) = init_snr;
                    case num2cell(numRev-revInt+1:numRev)
                        rev(reversenumber-(numRev-revInt)) = init_snr;
                end %end of switch reverenumber sentence
            end
        end
        correct = 0;
        fprintf(fid,'%d                   0          \r\n', init_snr);
        
        init_snr = init_snr + 2;
        if init_snr > 50
            init_snr = 50;
        end
        
        if init_snr < -100
            init_snr = -100;
        end
        
    end %end of if sentence
end %end of while sentence
delete(handles.figure1);
clear('hObject','handles');

result.total = [before_rev;rev]; %for plotting
result.rev = rev; %for calculating
result.mean = mean(rev);
result.std = std(rev);
result.go = go;
result.count = count;
fclose(fid);
end