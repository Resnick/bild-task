% raw_Audio_Dir = 'E:\Users\Jesse\Documents\bild-task\AuxFiles';
raw_Audio_Dir = 'C:\Users\jsere\Documents\BILDtask\AuxFiles';
% reverb_Audio_Dir = 'E:\Users\Jesse\Documents\bild-task\Reverb\Reverb Stims';
reverb_Audio_Dir = 'C:\Users\jsere\Documents\BILDtask\Reverb\Reverb Stims';

roomTypes = {'anechoic','meeting','auditorium'};

%% Generate convolved noise
noiseFileName = 'noise';
for roomIDX = 1:length(roomTypes)
    room = roomTypes{roomIDX};
    inputWAVfile = [raw_Audio_Dir,filesep,noiseFileName,'.wav'];
    outputWAVfile = [reverb_Audio_Dir,filesep,noiseFileName,'_',room,'.wav'];
    gen_Reverb_Stim(inputWAVfile,outputWAVfile,room);
    sprintf('Generated %s\n',outputWAVfile)
end
%% Generate ramped, then convolved, noise
noiseFileName = 'noise';
inputWAVfile = [raw_Audio_Dir,filesep,noiseFileName,'.wav'];
rawNoise = audioread(inputWAVfile);
ramp = ones(length(rawNoise),1);
rampDur = 50*1e-3*fs;
ramp(1:rampDur) = (0:rampDur-1)/rampDur;
ramp(end-rampDur+1:end) = ((rampDur-1):-1:0)/rampDur;
rampedNoise = rawNoise.*ramp;
audiowrite([reverb_Audio_Dir,filesep,'rampedNoise.wav'],rampedNoise,fs);

for roomIDX = 1:length(roomTypes)
    room = roomTypes{roomIDX};
    inputWAVfile = [reverb_Audio_Dir,filesep,'rampedNoise.wav'];
    outputWAVfile = [reverb_Audio_Dir,filesep,'rampedNoise_',room,'.wav'];
    gen_Reverb_Stim(inputWAVfile,outputWAVfile,room);
    sprintf('Generated %s\n',outputWAVfile)
end

%% Generate convolved calibration tone
calibFileName = 'calib_tone';
for roomIDX = 1:length(roomTypes)
    room = roomTypes{roomIDX};
    inputWAVfile = [raw_Audio_Dir,filesep,calibFileName,'.wav'];
    outputWAVfile = [reverb_Audio_Dir,filesep,calibFileName,'_',room,'.wav'];
    gen_Reverb_Stim(inputWAVfile,outputWAVfile,room);
    sprintf('Generated %s\n',outputWAVfile)
end
%% Generate convolved speech tokens.
speech_Tokens = {'birthday','drawbridge','eardrum','iceberg','mousetrap',...
    'northwest','padlock','playground','sidewalk','stairway','toothbrush',...
    'woodwork'};

for roomIDX = 1:length(roomTypes)
    room = roomTypes{roomIDX};
for tokenIDX = 1:length(speech_Tokens)
    inputWAVfile = [raw_Audio_Dir,speech_Tokens{tokenIDX},'.wav'];
    outputWAVfile = [reverb_Audio_Dir,speech_Tokens{tokenIDX},'_',room,'.wav'];
    gen_Reverb_Stim(inputWAVfile,outputWAVfile,room);
    sprintf('Generated %s\n',outputWAVfile)
end
end

