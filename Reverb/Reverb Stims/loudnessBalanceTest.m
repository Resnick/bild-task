bildAuxDir = '..\..\AuxFiles\';

birthday = audioread([bildAuxDir, 'birthday.wav']); birthday = birthday(:,1);
drawbridge = audioread([bildAuxDir, 'drawbridge.wav']); drawbridge = drawbridge(:,1);
eardrum = audioread([bildAuxDir, 'eardrum.wav']); eardrum = eardrum(:,1);
iceburg = audioread([bildAuxDir, 'iceberg.wav']); iceburg = iceburg(:,1);
mousetrap = audioread([bildAuxDir, 'mousetrap.wav']); mousetrap = mousetrap(:,1);
northwest = audioread([bildAuxDir, 'northwest.wav']); northwest = northwest(:,1);
padlock = audioread([bildAuxDir, 'padlock.wav']); padlock = padlock(:,1);
playground = audioread([bildAuxDir, 'playground.wav']); playground = playground(:,1);
sidewalk = audioread([bildAuxDir, 'sidewalk.wav']); sidewalk = sidewalk(:,1);
stairway = audioread([bildAuxDir, 'stairway.wav']); stairway = stairway(:,1);
toothbrush = audioread([bildAuxDir, 'toothbrush.wav']); toothbrush = toothbrush(:,1);
woodwork = audioread([bildAuxDir, 'woodwork.wav']); woodwork = woodwork(:,1);

%%
reverbAuxDir = '..\..\Reverb\Reverb_Aux\';

birthday_anechoic = audioread([reverbAuxDir, 'birthday_anechoic.wav']); birthday_anechoic = birthday_anechoic(:,1);
drawbridge_anechoic = audioread([reverbAuxDir, 'drawbridge_anechoic.wav']); drawbridge_anechoic = drawbridge_anechoic(:,1);
eardrum_anechoic = audioread([reverbAuxDir, 'eardrum_anechoic.wav']); eardrum_anechoic = eardrum_anechoic(:,1);
iceburg_anechoic = audioread([reverbAuxDir, 'iceberg_anechoic.wav']); iceburg_anechoic = iceburg_anechoic(:,1);
mousetrap_anechoic = audioread([reverbAuxDir, 'mousetrap_anechoic.wav']); mousetrap_anechoic = mousetrap_anechoic(:,1);
northwest_anechoic = audioread([reverbAuxDir, 'northwest_anechoic.wav']); northwest_anechoic = northwest_anechoic(:,1);
padlock_anechoic = audioread([reverbAuxDir, 'padlock_anechoic.wav']); padlock_anechoic = padlock_anechoic(:,1);
playground_anechoic = audioread([reverbAuxDir, 'playground_anechoic.wav']); playground_anechoic = playground_anechoic(:,1);
sidewalk_anechoic = audioread([reverbAuxDir, 'sidewalk_anechoic.wav']); sidewalk_anechoic = sidewalk_anechoic(:,1);
stairway_anechoic = audioread([reverbAuxDir, 'stairway_anechoic.wav']); stairway_anechoic = stairway_anechoic(:,1);
toothbrush_anechoic = audioread([reverbAuxDir, 'toothbrush_anechoic.wav']); toothbrush_anechoic = toothbrush_anechoic(:,1);
woodwork_anechoic = audioread([reverbAuxDir, 'woodwork_anechoic.wav']); woodwork_anechoic = woodwork_anechoic(:,1);

%%
birthday_meeting = audioread([reverbAuxDir, 'birthday_meeting.wav']); birthday_meeting = birthday_meeting(:,1);
drawbridge_meeting = audioread([reverbAuxDir, 'drawbridge_meeting.wav']); drawbridge_meeting = drawbridge_meeting(:,1);
eardrum_meeting = audioread([reverbAuxDir, 'eardrum_meeting.wav']); eardrum_meeting = eardrum_meeting(:,1);
iceburg_meeting = audioread([reverbAuxDir, 'iceberg_meeting.wav']); iceburg_meeting = iceburg_meeting(:,1);
mousetrap_meeting = audioread([reverbAuxDir, 'mousetrap_meeting.wav']); mousetrap_meeting = mousetrap_meeting(:,1);
northwest_meeting = audioread([reverbAuxDir, 'northwest_meeting.wav']); northwest_meeting = northwest_meeting(:,1);
padlock_meeting = audioread([reverbAuxDir, 'padlock_meeting.wav']); padlock_meeting = padlock_meeting(:,1);
playground_meeting = audioread([reverbAuxDir, 'playground_meeting.wav']); playground_meeting = playground_meeting(:,1);
sidewalk_meeting = audioread([reverbAuxDir, 'sidewalk_meeting.wav']); sidewalk_meeting = sidewalk_meeting(:,1);
stairway_meeting = audioread([reverbAuxDir, 'stairway_meeting.wav']); stairway_meeting = stairway_meeting(:,1);
toothbrush_meeting = audioread([reverbAuxDir, 'toothbrush_meeting.wav']); toothbrush_meeting = toothbrush_meeting(:,1);
woodwork_meeting = audioread([reverbAuxDir, 'woodwork_meeting.wav']); woodwork_meeting = woodwork_meeting(:,1);

%%
birthday_auditorium = audioread([reverbAuxDir, 'birthday_auditorium.wav']); birthday_auditorium = birthday_auditorium(:,1);
drawbridge_auditorium = audioread([reverbAuxDir, 'drawbridge_auditorium.wav']); drawbridge_auditorium = drawbridge_auditorium(:,1);
eardrum_auditorium = audioread([reverbAuxDir, 'eardrum_auditorium.wav']); eardrum_auditorium = eardrum_auditorium(:,1);
iceburg_auditorium = audioread([reverbAuxDir, 'iceberg_auditorium.wav']); iceburg_auditorium = iceburg_auditorium(:,1);
mousetrap_auditorium = audioread([reverbAuxDir, 'mousetrap_auditorium.wav']); mousetrap_auditorium = mousetrap_auditorium(:,1);
northwest_auditorium = audioread([reverbAuxDir, 'northwest_auditorium.wav']); northwest_auditorium = northwest_auditorium(:,1);
padlock_auditorium = audioread([reverbAuxDir, 'padlock_auditorium.wav']); padlock_auditorium = padlock_auditorium(:,1);
playground_auditorium = audioread([reverbAuxDir, 'playground_auditorium.wav']); playground_auditorium = playground_auditorium(:,1);
sidewalk_auditorium = audioread([reverbAuxDir, 'sidewalk_auditorium.wav']); sidewalk_auditorium = sidewalk_auditorium(:,1);
stairway_auditorium = audioread([reverbAuxDir, 'stairway_auditorium.wav']); stairway_auditorium = stairway_auditorium(:,1);
toothbrush_auditorium = audioread([reverbAuxDir, 'toothbrush_auditorium.wav']); toothbrush_auditorium = toothbrush_auditorium(:,1);
woodwork_auditorium = audioread([reverbAuxDir, 'woodwork_auditorium.wav']); woodwork_auditorium = woodwork_auditorium(:,1);

%%
figure;
plot(birthday); hold on;
plot(drawbridge);
plot(eardrum);
plot(iceburg);
plot(mousetrap);
plot(northwest);
plot(padlock);
plot(playground);
plot(sidewalk);
plot(stairway);
plot(toothbrush);
plot(woodwork);

%%
spondees = {birthday,drawbridge,eardrum,iceburg,mousetrap,northwest,...
    padlock,playground,sidewalk,stairway,toothbrush,woodwork};
spondees_anechoic = {birthday_anechoic,drawbridge_anechoic,eardrum_anechoic,...
    iceburg_anechoic,mousetrap_anechoic,northwest_anechoic,...
    padlock_anechoic,playground_anechoic,sidewalk_anechoic,...
    stairway_anechoic,toothbrush_anechoic,woodwork_anechoic};
spondees_auditorium = {birthday_auditorium,drawbridge_auditorium,eardrum_auditorium,...
    iceburg_auditorium,mousetrap_auditorium,northwest_auditorium,...
    padlock_auditorium,playground_auditorium,sidewalk_auditorium,...
    stairway_auditorium,toothbrush_auditorium,woodwork_auditorium};
spondees_meeting = {birthday_meeting,drawbridge_meeting,eardrum_meeting,...
    iceburg_meeting,mousetrap_meeting,northwest_meeting,...
    padlock_meeting,playground_meeting,sidewalk_meeting,...
    stairway_meeting,toothbrush_meeting,woodwork_meeting};
%% 
clear RMSs RMSs_anechoic RMSs_auditorium RMSs_meeting
for i = 1:length(spondees)
    lengthSpondees(i) = length(spondees{i});
    lengthSpondees_anechoic(i) = length(spondees_anechoic{i});
    lengthSpondees_auditorium(i) = length(spondees_auditorium{i});
    lengthSpondees_meeting(i) = length(spondees_meeting{i});
    RMSs(i) = std(spondees{i}(:,1));
    RMSs_anechoic(i) = std(spondees_anechoic{i}(:,1));
    RMSs_auditorium(i) = std(spondees_auditorium{i}(:,1));
    RMSs_meeting(i) = std(spondees_meeting{i}(:,1));
end
figure
scatter(ones(1,length(RMSs)),RMSs,'b'); hold on
scatter(2*ones(1,length(RMSs_anechoic)),RMSs_anechoic,'b');
scatter(3*ones(1,length(RMSs_auditorium)),RMSs_auditorium,'b');
scatter(4*ones(1,length(RMSs_meeting)),RMSs_meeting,'b');
meanLength = mean(lengthSpondees);
meanRMS = mean(RMSs);
meanRMS_anechoic = mean(RMSs_anechoic);
meanRMS_auditorium = mean(RMSs_auditorium);
meanRMS_meeting = mean(RMSs_meeting);

%% Try modifying with factors
factorAnechoic = meanRMS/meanRMS_anechoic
factorMeeting = meanRMS/meanRMS_auditorium
factorAuditorium = meanRMS/meanRMS_meeting
for i = 1:length(spondees)
    RMSs_anechoic2(i) = std(spondees_anechoic{i}(:,1)*factorAnechoic);
    RMSs_auditorium2(i) = std(spondees_auditorium{i}(:,1)*factorAuditorium);
    RMSs_meeting2(i) = std(spondees_meeting{i}(:,1)*factorMeeting);
end

figure;
scatter(ones(1,length(RMSs)),RMSs,'b'); hold on
scatter(2*ones(1,length(RMSs_anechoic2)),RMSs_anechoic2,'b');
scatter(3*ones(1,length(RMSs_auditorium2)),RMSs_auditorium2,'b');
scatter(4*ones(1,length(RMSs_meeting2)),RMSs_meeting2,'b');
ylabel('Spondee RMS');
xticks(1:4); xlim([0.5,4.5]);
xticklabels({'BILD','Anechoic','Auditorium','Meeting'});
%% 

figure;
plot(birthday_meeting(:,1)); hold on;
plot(birthday_auditorium(:,1));
plot(birthday_anechoic(:,1));
plot(birthday);
std_birthday = std(birthday);
std_birthday_anechoic = std(birthday_anechoic(:,1));
std_birthday_auditorium = std(birthday_auditorium(:,1));
std_birthday_meeting = std(birthday_meeting(:,1));

% factorAnechoic = std_birthday/std_birthday_anechoic
% factorMeeting = std_birthday/std_birthday_meeting
% factorAuditorium = std_birthday/std_birthday_auditorium