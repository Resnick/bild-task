function varargout = continue_Prompt(varargin)
% continue_Prompt MATLAB code for continue_Prompt.fig
%      continue_Prompt, by itself, creates a new continue_Prompt or raises the existing
%      singleton*.
%
%      H = continue_Prompt returns the handle to a new continue_Prompt or the handle to
%      the existing singleton*.
%
%      continue_Prompt('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in continue_Prompt.M with the given input arguments.
%
%      continue_Prompt('Property','Value',...) creates a new continue_Prompt or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before continue_Prompt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to continue_Prompt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help continue_Prompt

% Last Modified by GUIDE v2.5 30-Mar-2018 13:10:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @continue_Prompt_OpeningFcn, ...
                   'gui_OutputFcn',  @continue_Prompt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before continue_Prompt is made visible.
function continue_Prompt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to continue_Prompt (see VARARGIN)

% Choose default command line output for continue_Prompt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
uiwait(handles.figure1)


% --- Outputs from this function are returned to the command line.
function varargout = continue_Prompt_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
delete(handles.figure1);


% --- Executes on button press in continueButton.
function continueButton_Callback(hObject, eventdata, handles)
% hObject    handle to continueButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiresume(handles.figure1)
