function varargout = response(varargin)
% RESPONSE M-file for response.fig
%      RESPONSE by itself, creates a new RESPONSE or raises the
%      existing singleton*.
%
%      H = RESPONSE returns the handle to a new RESPONSE or the handle to
%      the existing singleton*.
%
%      RESPONSE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RESPONSE.M with the given input arguments.
%
%      RESPONSE('Property','Value',...) creates a new RESPONSE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before response_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to response_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help response

% Last Modified by GUIDE v2.5 26-Feb-2018 10:35:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @response_OpeningFcn, ...
    'gui_OutputFcn',  @response_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before response is made visible.
function response_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to response (see VARARGIN)

% Choose default command line output for response
beep off
set(hObject,'Color','k')
% Update handles structure
guidata(hObject, handles);

% Determine the position of the dialog - centered on the callback figure
% if available, else, centered on the screen
FigPos=get(0,'DefaultFigurePosition');
OldUnits = get(hObject, 'Units');
set(hObject, 'Units', 'pixels');
OldPos = get(hObject,'Position');
FigWidth = OldPos(3);
FigHeight = OldPos(4);
if isempty(gcbf)
    ScreenUnits=get(0,'Units');
    set(0,'Units','pixels');
    ScreenSize=get(0,'ScreenSize');
    set(0,'Units',ScreenUnits);
    
    FigPos(1)=1/2*(ScreenSize(3)-FigWidth);
    FigPos(2)=2/3*(ScreenSize(4)-FigHeight);
else
    GCBFOldUnits = get(gcbf,'Units');
    set(gcbf,'Units','pixels');
    GCBFPos = get(gcbf,'Position');
    set(gcbf,'Units',GCBFOldUnits);
    FigPos(1:2) = [(GCBFPos(1) + GCBFPos(3) / 2) - FigWidth / 2, ...
        (GCBFPos(2) + GCBFPos(4) / 2) - FigHeight / 2];
end
FigPos(3:4)=[FigWidth FigHeight];
set(hObject, 'Position', FigPos);
set(hObject, 'Units', OldUnits);

% --- Outputs from this function are returned to the command line.
function varargout = response_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;
varargout{2} = handles.figure1;


function varargout = resume_Callback(hObject,eventdata,handles)
set(handles.TitleBox,'String','Select the word you heard!');
set(handles.TitleBox,'ForegroundColor',[0,1,0]);
uiwait(handles.figure1);
handles = guidata(hObject);
if strcmp(handles.output,'abort')
    delete(handles.figure1)
else
    uiresume(handles.figure1)
end
varargout{1} = handles;
varargout{2} = handles.figure1;


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(handles.figure1, 'waitstatus'), 'waiting')
    handles.finish = imread(['AuxFiles',filesep,'exit.bmp']);
    handles.output = 'abort';
    guidata(hObject, handles);
    
    beep off
    hfig = figure('Units','normalized','Position',[0.35 0.4 0.21 0.28]);
    set(hfig,'MenuBar','none')
    set(hfig,'Color',[178/255 178/255 178/255])
    image(handles.finish);
    axis off
    uiresume(handles.figure1)
else
    delete(handles.figure1)
end

% --- Executes on key press over figure1 with no controls selected.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Check for "enter" or "escape"
if isequal(get(handles.figure1, 'waitstatus'), 'waiting')
    if isequal(get(hObject,'CurrentKey'),'escape')
        % User said no by hitting escape
        handles.output = 'abort';
        handles.finish = imread(['AuxFiles',filesep,'exit.bmp']);
        % Update handles structure
        guidata(hObject, handles);
        beep off
        hfig = figure('Units','normalized','Position',[0.35 0.4 0.21 0.28]);
        set(hfig,'MenuBar','none')
        set(hfig,'Color',[178/255 178/255 178/255])
        image(handles.finish);
        axis off
        uiresume(handles.figure1);
    end
else
    delete(handles.figure1)
end

% --- Executes on button press in BirthdayButton1. %BirthdayButton1
function BirthdayButton1_Callback(hObject, eventdata, handles)
% hObject    handle to BirthdayButton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');
% Update handles structure

guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in DrawbridgeButton2. %Drawbridge
function DrawbridgeButton2_Callback(hObject, eventdata, handles)
% hObject    handle to DrawbridgeButton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in EardrumButton3. %Eardrum
function EardrumButton3_Callback(hObject, eventdata, handles)
% hObject    handle to EardrumButton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in IcebergButton4. %Iceberg
function IcebergButton4_Callback(hObject, eventdata, handles)
% hObject    handle to IcebergButton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
uiresume(handles.figure1);

% --- Executes on button press in MousetrapButton5. %Mousetrap
function MousetrapButton5_Callback(hObject, eventdata, handles)
% hObject    handle to MousetrapButton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in NorthwestButton6. %Northwest
function NorthwestButton6_Callback(hObject, eventdata, handles)
% hObject    handle to NorthwestButton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in PadlockButton7. %Padlock
function PadlockButton7_Callback(hObject, eventdata, handles)
% hObject    handle to PadlockButton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in Playgroundbutton8. %Playground
function Playgroundbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to Playgroundbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in SidewalkButton9. %Sidewalk
function SidewalkButton9_Callback(hObject, eventdata, handles)
% hObject    handle to SidewalkButton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in Stairwaybutton10. %Stairway
function Stairwaybutton10_Callback(hObject, eventdata, handles)
% hObject    handle to Stairwaybutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in ToothbrushButton11. %Toothbrush
function ToothbrushButton11_Callback(hObject, eventdata, handles)
% hObject    handle to ToothbrushButton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes on button press in WoodworkButton12. %Woodwork
function WoodworkButton12_Callback(hObject, eventdata, handles)
% hObject    handle to WoodworkButton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.output = get(hObject,'Tag');

% Update handles structure
guidata(hObject, handles);

% Use UIRESUME instead of delete because the OutputFcn needs
% to get the updated handles structure.
set(handles.TitleBox,'String','Wait for the next sound...');
set(handles.TitleBox,'ForegroundColor',[1,0,0]);
uiresume(handles.figure1);

% --- Executes during object creation, after setting all properties.
function BirthdayButton1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BirthdayButton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a1 = imread(['AuxFiles',filesep,'w_birthday.bmp']);
set(hObject,'CData',handles.w_a1)

% --- Executes during object creation, after setting all properties.
function DrawbridgeButton2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DrawbridgeButton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a2 = imread(['AuxFiles',filesep,'w_drawbridge.bmp']);
set(hObject,'CData',handles.w_a2)

% --- Executes during object creation, after setting all properties.
function EardrumButton3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to EardrumButton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a3 = imread(['AuxFiles',filesep,'w_eardrum.bmp']);
set(hObject,'CData',handles.w_a3)

% --- Executes during object creation, after setting all properties.
function IcebergButton4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to IcebergButton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a4 = imread(['AuxFiles',filesep,'w_iceberg.bmp']);
set(hObject,'CData',handles.w_a4)

% --- Executes during object creation, after setting all properties.
function MousetrapButton5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MousetrapButton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a5 = imread(['AuxFiles',filesep,'w_mousetrap.bmp']);
set(hObject,'CData',handles.w_a5)

% --- Executes during object creation, after setting all properties.
function NorthwestButton6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NorthwestButton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a6 = imread(['AuxFiles',filesep,'w_northwest.bmp']);
set(hObject,'CData',handles.w_a6)

% --- Executes during object creation, after setting all properties.
function PadlockButton7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PadlockButton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a7 = imread(['AuxFiles',filesep,'w_padlock.bmp']);
set(hObject,'CData',handles.w_a7)

% --- Executes during object creation, after setting all properties.
function Playgroundbutton8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Playgroundbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a8 = imread(['AuxFiles',filesep,'w_playground.bmp']);
set(hObject,'CData',handles.w_a8)

% --- Executes during object creation, after setting all properties.
function SidewalkButton9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SidewalkButton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a9 = imread(['AuxFiles',filesep,'w_sidewalk.bmp']);
set(hObject,'CData',handles.w_a9)

% --- Executes during object creation, after setting all properties.
function Stairwaybutton10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Stairwaybutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a10 = imread(['AuxFiles',filesep,'w_stairway.bmp']);
set(hObject,'CData',handles.w_a10)

% --- Executes during object creation, after setting all properties.
function ToothbrushButton11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ToothbrushButton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a11 = imread(['AuxFiles',filesep,'w_toothbrush.bmp']);
set(hObject,'CData',handles.w_a11)

% --- Executes during object creation, after setting all properties.
function WoodworkButton12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WoodworkButton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.w_a12 = imread(['AuxFiles',filesep,'w_woodwork.bmp']);
set(hObject,'CData',handles.w_a12)

% --- Executes on button press in StopButton.
function StopButton_Callback(hObject, eventdata, handles)
% hObject    handle to StopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%STOP!

if isequal(get(handles.figure1, 'waitstatus'), 'waiting')
    % User said no by hitting stop pushbutton
    handles.output = 'abort';
    % Update handles structure
    guidata(hObject, handles);
    handles.finish = imread(['AuxFiles',filesep,'exit.bmp']);
    beep off
    hfig = figure('Units','normalized','Position',[0.35 0.4 0.21 0.28]);
    set(hfig,'MenuBar','none')
    set(hfig,'Color',[178/255 178/255 178/255])
    image(handles.finish);
    axis off
    uiresume(handles.figure1)
end


